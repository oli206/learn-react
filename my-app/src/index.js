import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a]
      && squares[a].value
      && squares[a].value === squares[b].value
      && squares[a].value === squares[c].value
    ) {
      squares[a].isWinner = true;
      squares[b].isWinner = true;
      squares[c].isWinner = true;
      return squares[a].value;
    }
  }

  return null;
}

function Square(props) {
  return (
    <button
      className="square"
      onClick={props.onClick}
      style={props.highlight ? { color: 'red' } : {}}
    >
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i].value}
        onClick={() => this.props.onClick(i)}
        highlight={this.props.squares[i].isWinner}
      />
    );
  }

  renderRow(rowIndex) {
    const row = [];
    for (let columnIndex = 0; columnIndex < 3; columnIndex++) {
      row.push(this.renderSquare(columnIndex + (3 * rowIndex)));
    }
    return (
      <div className="board-row">
        {row}
      </div>
    );
  }

  render() {  
    const table = [];
    for (let rowIndex = 0; rowIndex < 3; rowIndex++) {
      table.push(this.renderRow(rowIndex));
    }

    return (
      <div>
        {table}
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill().map( o => ({value: null, isWinner: false})),      
        lastMove: "",        
      }],
      stepNumber: 0,
      xIsNext: true,
      selectedHistory: null
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1)
      .map(h => ({
        squares: h.squares.map(square => ({ value: square.value, isWinner: square.isWinner })),
        lastMove: h.lastMove
      }));

    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i].value) {
      return;
    }
    const col = i % 3;
    const row = Math.floor(i / 3);
    squares[i].value = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
        lastMove: "(" + col + ", " + row + ")",      
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      selectedHistory: current.selected
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
      selectedHistory: step
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move + " " + step.lastMove:
        'Go to game start' + " " + step.lastMove;
      return (
        <li key={move}>
          <button
            style={move === this.state.selectedHistory ? { fontWeight: 'bold' } : {}}
            onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
